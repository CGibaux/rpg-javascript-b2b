const RpgSpell = require('../RpgSpell.js')

/* eslint no-undef: "off" */
module.exports = class RpgSpellRekop extends RpgSpell {

    constructor(player) {
        let data = {
            mana: 200
        }
        super(player, data)
        //initialisation à 0, récupére la variable pour savoir pendant combien de tours l'attaque s'est chargée
        this.count = 0
    }


    cast(target) {
        this.player.logger(`${this.player.name} is charging Rekop`)
        target.addBeforeTurn(this.callback.bind(this))
        this.consume()
        return true

    }

    callback(target) {
        //init un nombre aléatoire en utilisant la méthode déjà implémentée
        let randInt = this.player.randPower(1, 4)
        this.count++

        //fin du spell
        if (this.count > 4) {
            return
        }

        //Au 4ème tour le spell se lance s'il n'est pas déclenché avant
        else if (this.count == 4) {
            this.damageRekop = this.player.randPower(250, 300) * this.count
            target.health -= this.damageRekop   
            target.logger(`${this.player.name} hit ${target.name} with a Rekop for ${this.damageRekop} damage charged during ${this.count} rounds`)
            this.count = 10
        }

        //Chaque tour le spell à une chance de se déclencher...
        else {
            if (randInt === 1) {
                this.damageRekop = this.player.randPower(200, 250) * this.count
                target.health -= this.damageRekop               
                target.logger(`${this.player.name} hit ${target.name} with a Rekop for ${this.damageRekop} damage charged during ${this.count} rounds`)
                this.count = 10
            }

            //...sinon il continu de se charger
            else {
                target.logger(`${this.player.name} is still charging his attack…`)
            }


        }

        return this.callback.bind(this)
    }

}