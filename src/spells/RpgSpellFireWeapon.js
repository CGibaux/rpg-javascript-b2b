/*  L'attaquant invoque une arme enflammée, il subit aussi un débuff de "dexterity" de 30%.
    Si il réussis son attaque, l'adversaire perd 10% de ses points de vie par tour pendant 3 tours.
    Si il rate son attaque, c'est lui (l'attaquant) qui perd 10% de ses points de vie par tour pendant 3 tours.
    L'adversaire NE PEUT PAS esquiver cette attaque */

    const RpgSpell = require('../RpgSpell.js')

    module.exports = class RpgSpellFireWeapon extends RpgSpell {
    
        constructor(player) {
            let data = {
                mana: 80
            }
            super(player, data)
            this.count = 1
            // Définit les dégats que fait l'attaque
            this.damage = this.player.randPower((this.player.strength * 0.7) * 10, this.player.attack)
        }
    
        cast(target) {
    
            this.consume()
    
            this.player.logger(`${this.player.name} invokes a fiery sword!`)
            
            // Ajout du debuff de dexterité (50%) sur l'attaquant 
            let dexterityDebuff = this.player.dexterity * -0.5
            this.player.addBuff('dexterity', dexterityDebuff, 1)
    
            if (this.player.calculateMiss()) {
                this.player.logger(`${this.player.name} missed ${target.name}!`)
                this.player.logger(`${this.player.name} catches fire`)
                // L'attaquant rate son attaque : il prend feu
                this.player.addBeforeTurn(this.callback.bind(this))
            }
            else {
                this.player.logger(`${this.player.name} hit ${target.name}! ${target.name} lose ${this.damage} HP.`)
                this.player.logger(`${target.name} catches fire`)
                // L'attaquant réussis son attaque : il touche son adversaire et le met en feu
                target.health -= this.damage
                target.addBeforeTurn(this.callback.bind(this))
            }
    
        }
    
        callback(target) {
    
            this.count++
    
            // Si 3 tours passé ou joueur mort, il arrête de brûler
            if (this.count > 4 || target.health <= 0) {
                target.logger('end burns')
                return
            }

            // Joueur perd 10% de sa vie actuelle
            let damage = target.health * 0.1
            target.logger(`${target.name} burns`)
            target.logger(`${target.name} loose ${damage} HP`)
            target.health -= this.damage
    
            return this.callback.bind(this)
        }
    
    }