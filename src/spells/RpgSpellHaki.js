const RpgSpell = require('../RpgSpell.js')

/* eslint no-undef: "off" */
module.exports = class RpgSpellHaki extends RpgSpell {
    

    constructor(player) {
        let data = {
            mana: 300
        }
        super(player, data)
    }

    cast(target) {
        let randhaki = this.player.randPower(1, 3)
        if(randhaki == 1){
            this.consume()
            let damage = this.player.randPower(300, 400)
            target.health -= damage
            this.player.logger(`${this.player.name} hit ${target.name} with Busoshoku Haki  for ${damage} damage`)
        }
        else if(randhaki == 2){
            this.consume()
            this.player.addBuff('strength', 4, 2)
            this.player.addBuff('dexterity', 4, 2)
            this.player.addBuff('speed', 4, 2)
            this.player.addBuff('intelligence', 4, 2)
            this.player.logger(`${this.player.name} gains 4 strength, dexterity, speed, intelligence with Kenbunshoku no Haki`)
        }
        else{
            this.consume()
            target.addBuff('strength', -15, 1)
            let damage = this.player.randPower(40, 60)
            target.health -= damage
            this.player.logger(`${this.player.name} lower 15 strength to ${target.name} and hit him with Haoshoku Haki for ${damage} damage`)
        }   
    }
}