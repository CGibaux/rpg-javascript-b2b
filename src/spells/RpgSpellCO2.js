const RpgSpell = require('../RpgSpell');

/* eslint no-undef: "off" */
module.exports = class RpgSpellCO2 extends RpgSpell{
    constructor(player) {
        let data = {
            mana: 200
        };
        super(player, data);

        this.count = 0;
    }

    cast(target) {
        let damage = this.player.randPower(300, 350);
        target.health -= damage;
        this.player.logger(`${this.player.name} Envoie un nuage de CO2 vers son adversaire ${target.name} et inflige ${damage} de dégâts`);
        target.addBeforeTurn(this.callback.bind(this))
    }

    callback(player) {
        this.count++;

        if (this.count > 3) {
            this.player.logger(`La loi sur la COP 21 est passé ce qui entraîne la démission de ${this.player.name}`);
            return
        }

        this.consume();
        let damage = 350;
        player.health -= damage;
        player.logger(`${player.name} Subit ${damage} de dégâts du à la polution`);
        return this.callback.bind(this);
    }
};