const RpgSpell = require('../RpgSpell.js')

/* eslint no-undef: "off" */
module.exports = class RpgSpellMetallica extends RpgSpell {

    constructor(player) {
        let data = {
            mana: 200
        }
        super(player, data)
        this.count = 1
    }

    cast() {

        this.consume()

        this.player.addBeforeTurn(this.callback.bind(this))
        this.player.logger(`${this.player.name} cast metallica on itself, granting infinite defense but cannot act for 2 turns`)
        this.player.addBuff("strength",999,2)
        //this.player.addBuff("constitution",999,2)//
        return true
    }

    callback() {

        
        let healing = 2/100*(this.player.health)
        this.count++
        if (this.count > 3) {
            this.player.logger(`${this.player.name} demetalized itself, he can act again`)
            return
        }

        if (this.player.health < this.player.baseHealth){
            this.player.health += healing
        
        }
        
        
        this.player.logger(`${this.player.name} cannot move!`)
        this.player.cancelAttack()
        this.player.logger(`${this.player.name} Healing itself for ${healing} HP by staying metalized`)
 
        

        return this.callback.bind(this)
    }

}
