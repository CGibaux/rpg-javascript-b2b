/*  Ce sort fait plus de dégat si la vie du joueur est faible.
*   Si rate l'attaque le joueur est étourdis.
*/

const RpgSpell = require('../RpgSpell.js')

module.exports = class RpgSpellFinisher extends RpgSpell {
    constructor(player) {
        let data = {
            mana: 100
        }
        super(player, data)
        this.count = 0
    }

    cast(target) {
        this.consume()

        if (target.calculateDodge()) {
            this.player.logger(`${this.player.name} attack ${target.name} but he dodge...`)
            this.player.addBeforeTurn(this.callback.bind(this))
            return true
        }

        let health = this.player.health
        let baseHealth = this.player.baseHealth

        let damage = this.player.randPower(this.player.attack + 100, this.player.attack + 180) - target.defense
        //Multiplie les damages par 1.2 plus le pourcentage de pv enlevé
        damage *= 1.2 + ((this.player.baseHealth - this.player.health) / this.player.baseHealth)

        target.health -= damage

        this.player.logger(`${this.player.name} hit ${target.name} 
            for ${damage} damage (${target.health}/${target.baseHealth})...`)
        return true 
    }

    callback(target) {
        this.count++

        if (this.count > 1) {
            target.logger(`${this.player.name} is no more stun...`)
            return true
        }

        this.player.logger(`${this.player.name} is stun...`)
        this.player.cancelAttack()

        return this.callback.bind(this)
    }
}
