// Two statistics : Luck and Intelligence
// Calculation : intelligenceStat = Your luck stat is multipliate with a random number (luckStat) included between 1 and 10 and split by 10 (It's like an average) (It's the same calculation for the intelligneceStat)
// The 2 calculation are added and if :
//  - The result is greater than or equal to 40 -> The target lose randPower between 300 and 400 and your buffs are removed (The lightning storm touches the target)
//  - The result is less than 40 -> You lose your randPower between 300 and 400 and  your buffs are removed (The lightning storm touches you)
// You can miss your spell (nothing happens) or do a critical spell and damages are multiplicated by 1.2

const RpgSpell = require('../RpgSpell.js')

/* eslint no-undef: "off" */
module.exports = class RpgSpellParalysis extends RpgSpell {
    constructor(player){
        let data={
            mana:100
        }
        super(player, data)
        this.count = 0
    }

    cast(target){
        this.consume();

        let luckStat = Math.floor( Math.random() * 10 ) + 1;
        let intelligenceStat = Math.floor( Math.random() * 10 ) + 1;
        let result = (this.player.luck * luckStat / 10) + (this.player.intelligence * intelligenceStat / 10)
        let damage = this.player.randPower(300, 400)

        if(this.player.calculateMiss()){
            this.player.logger(`${this.player.name} don't know dancing, he miss his ThunderDance spell`)
            return true
        }
        else if(this.player.calculateCritical()){
            this.player.logger(`${this.player.name} is the king of the dance, a torrential storm falls`)
            if(result<40){
                this.player.logger(`A lightining storm hurts ${this.player.name}, he losed ${damage} HP and he cannot move!`)
                this.player.health -= (damage*=1.2)
                return true
            }
            else{
                target.logger(`A lightining storm hurts ${target.name}, he losed ${damage} HP and he cannot move!`)
                target.health -= (damage*=1.2)
                return true
            }
        }
        else {
            if(result<40){
                this.player.addBeforeTurn(this.callback.bind(this))
                this.player.logger(`A lightining storm hurts ${this.player.name}, he losed ${damage} HP and he cannot move!`)
                this.player.health -= damage
                return true
            }
            else{
                target.addBeforeTurn(this.callback.bind(this))
                target.logger(`A lightining storm hurts ${target.name}, he losed ${damage} HP and he cannot move!`)
                target.health -= damage
                return true
            }
        }

    }

    //Resumption and modification of the part callnack in the code 'RpgSpellParalysis'

    callback(target) {

        this.count++
        let electrocuted = 2 - this.count
        if (this.count > 1) {
            target.logger(`${target.name} can move now`)
            return
        }

        target.logger(`${target.name} is electrocuted, he lose 50HP and he cannot move during ${electrocuted} turn!`)
        target.health -= 50
        target.cancelAttack()

        return this.callback.bind(this)
    }
}