const RPGCharacter = require('../index.js');

const nathalieName = ['Nathalie Kosciusko-Morizet'];

const nathalieStats = {
    strength: 8, dexterity: 12, intelligence: 3, constitution: 1000,
    wisdom: 3, luck: 10, speed: 4
};

const valerieName = ['Valérie Pécrésse'];

const valerieStats = {
    strength: 1, dexterity: 1, intelligence: 1, constitution: 1000,
    wisdom: 1, luck: 1, speed: 1
};
const nathalieSpell = ['CO2'];
const Test = [
    ['Valerie', 'warrior', valerieStats],
    ['Nathalie', 'mage', nathalieStats, nathalieSpell]
];
const specs = [
    ['strength'], ['dexterity'], ['intelligence'],
    ['luck'], ['speed'], ['wisdom'], ['constitution']
];

describe('RPGCharacter Constructor', () => {
    it('should register the name of the character', function () {
        let c = new RPGCharacter(nathalieName ,nathalieStats);
        expect(c.nathalieName).toEqual('Nathalie Kosciusko-Morizet')
    });

    it('should activate the spell', function () {
        let c = new RPGCharacter(nathalieName, nathalieStats, nathalieSpell);
        expect(c.hasSpell('CO2')).toBeTruthy()
    });

    it('should hit the target', function () {
        let c = new RPGCharacter(nathalieName, nathalieStats, nathalieSpell);
        let i = new RPGCharacter(valerieName, valerieStats);
        let damage = this.c.randPower(300, 500);
        i.health -= damage;
        this.player.logger(`${c.nathalieName} hit the target name ${i.valerieName} and deal ${damage} damage`)
        // this.player.logger(`${this.player.name} Envoie un nuage de CO2 vers son adversaire ${target.name} et inflige ${damage} de dégâts`);
    });
    it('should deal damage 3 round in a row', function () {
        let c = new RPGCharacter(nathalieName, nathalieStats, nathalieSpell);
        let i = new RPGCharacter(valerieName, valerieStats);
        this.count = 0;
        this.count++;

        if (this.count > 3){
            this.c.logger ('Fin des dégâts sur la durée');
            return
        }

        this.consume();
        let damage = 350;
        c.health -= damage;
        c.logger(`${c.nathalieName} Subit les dégâts équivalant à ${damage} sur la durée`);
        return this.callback.bind(this);
    });
});