const RpgCharacter = require("../index.js")

const baseData = {
    strength: 8, dexterity: 0, intelligence: 3, constitution: 10,
    wisdom: 3, luck: 10, speed: 4
}
const baseData2 = {
    strength: 1, dexterity: 0, intelligence: 1, constitution: 10,
    wisdom: 1, luck: 1, speed: 1
}
const spellData = ['Finisher']
const warrior = new RpgCharacter('Test1', 'warrior', baseData, spellData)
const mage = new RpgCharacter('Test2', 'mage', baseData2,['Icespear'])
const mage2 = new RpgCharacter('Test3', 'mage', {
    strength: 1, dexterity: 800, intelligence: 1, constitution: 10, wisdom: 1, luck: 1, speed: 1
})

describe('Finisher spell', () => {

    it('should consume the mana', () => {
        warrior.cast('Finisher', mage)
        expect(warrior.mana).toBeLessThan(warrior.baseMana)
    })

    it('should hit the mage', () => {
        warrior.cast('Finisher', mage)
        expect(mage.health).toBeLessThan(mage.baseHealth)
    })

    it('should stun the warrior', () =>  {
        warrior.cast('Finisher', mage2)
        expect(mage2.health).toEqual(mage2.baseHealth)
    })

    it('should hit harder', () => {
        warrior.health = 1
        warrior.cast('Finisher',mage)
        mageHealth1 = mage.health
        warrior.health = 4000
        warrior.cast('Finisher',mage)
        mageHealth2 = mage.health
        expect(mageHealth1).toBeGreaterThan(mageHealth2)
    })
})
