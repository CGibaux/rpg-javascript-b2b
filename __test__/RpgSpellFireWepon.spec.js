const RpgCharacter = require("../index.js")

// Test plantent certaines fois car j'aurais du utiliser des mockups
describe('FireWeapon Class', () => {

    it('mana consume', () => {
        let player = new RpgCharacter('player', 'mage', {
            strength: 1, dexterity: 1, intelligence: 1, constitution: 1000, wisdom: 1, luck: 1, speed: 1
        }, ['FireWeapon'])
        let target = new RpgCharacter('target', 'warrior', {
            strength: 8, dexterity: 12, intelligence: 3, constitution: 1000, wisdom: 3, luck: 10, speed: 4
        })
        player.cast('FireWeapon', target)
        expect(player.mana).toEqual(120)
    })

    it('give damage', () => {
        let player = new RpgCharacter('player', 'mage', {
            strength: 1, dexterity: 1, intelligence: 10, constitution: 1000, wisdom: 1, luck: 1, speed: 1
        }, ['FireWeapon'])
        let target = new RpgCharacter('target', 'warrior', {
            strength: 2, dexterity: 2, intelligence: 2, constitution: 10, wisdom: 2, luck: 2, speed: 2
        })
        player.cast('FireWeapon', target)
        expect(target.health).toBeLessThan(target.baseHealth)
    })

    it('spell give damage next round', () => {
        let player = new RpgCharacter('player', 'mage', {
            strength: 1, dexterity: 1, intelligence: 1, constitution: 1000, wisdom: 1, luck: 1, speed: 1
        }, ['FireWeapon'])
        let target = new RpgCharacter('target', 'warrior', {
            strength: 8, dexterity: 12, intelligence: 3, constitution: 1000, wisdom: 3, luck: 10, speed: 4
        })
        player.cast('FireWeapon', target)
        let healthBeforeRound = target.health
        target.melee(player)
        expect(target.health).toBeLessThan(healthBeforeRound)
    })

    // Dexterité revient à l'état de base à la fin du sort donc ne peux pas être testé (sans mockup)

})